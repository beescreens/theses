# BeePlace - Recréer l'expérience collaborative du r/place de Reddit

Valentin Kaelin, 2023

L'association Baleinev organise depuis près de 30 ans le festival de musique
Baleinev Festival sur le campus de la HEIG-VD. Depuis 2014, le festival propose
un concept innovant nommé Pimp My Wall, une application collaborative de dessins
en temps réel sur les fenêtres des tours de l'école.

Le concept de BeePlace se base sur la conclusion que Pimp My Wall est une
application trop permissive. Les utilisateurs peuvent dessiner de manière
entièrement libre sur la toile virtuelle et cela engendre quelques comportements
inappropriés.

En recréant l'expérience proposée par le r/place de Reddit, l'objectif est de
proposer une application web dans laquelle les utilisateurs peuvent créer des
uvres d'art collaboratives. Chaque utilisateur peut dessiner un pixel à la fois
sur la toile partagée et doit attendre un certain temps avant de pouvoir
dessiner à nouveau. Cela permet de forcer la collaboration et de limiter les
débordements.

Pour les festivaliers, l'application a été pensée en premier lieu pour une
utilisation mobile en faisant usage de technologies telles que Next.JS, NestJS
et Redis, parmi d'autres. Un des points principaux de ce travail, en plus de la
réalisation de l'application, était d'assurer la montée en charge afin de
garantir la fluidité de l'application en tout temps. Cela a pu être réalisé
grâce à k6 et Clinic.js, aboutissant à un gain de 142% de joueurs par rapport à
la version initiale, pour un total estimé de 1350 joueurs en parallèle.

Pour finir, cette application sera peaufinée et testée lors de la prochaine
édition du Baleinev Festival, afin de valider sa pertinence pour les
festivaliers et son comportement dans un contexte réel. L'application est d'ores
et déjà accessible à l'adresse
[place.beescreens.ch](https://place.beescreens.ch).

---

- Teacher in charge: Nastaran Fatemi
- Expert: Pierre-Benjamin Monaco
- Topic proposed by: Association Baleinev (Ludovic Delafontaine) and Valentin
- Official publication of the thesis (if applicable): N/A - Maybe later?
- BeeScreens documentation: N/A - Work in progress
- Documents
  - First formulation of specifications:
    [`first-formulation-of-specifications.pdf`](./first-formulation-of-specifications.pdf)
  - Intermediary report: [`intermediary-report.pdf`](./intermediary-report.pdf)
  - Final report: [`final-report.pdf`](./final-report.pdf)
  - Poster: [`poster.pdf`](./poster.pdf)
  - Presentation: [`presentation.pdf`](./presentation.pdf)
- Source code:
  - Thesis work:
    <https://gitlab.com/beescreens/beescreens/-/tree/main/apps/beeplace>
  - Thesis report: <https://github.com/heig-vkaelin/template-tb>
  - Thesis presentation: <https://github.com/heig-vkaelin/defense-tb>
- Feedback[^1]:
  - What was good:
    - Very well written and structured
    - Good amount of details without doing too much
    - Very educational and fun
    - The presentation was excellent
  - What could have been improved:
    - Be careful when discussing about numbers, statistics and the scientific
      approach (15000 ms could simply be 1.5s for instance)
  - Personal feedback and/or tips for future students:
    - LaTeX was a good choice in my opinion, I am really happy with the final
      result. However, it can be a bit hard to debug without previous
      experience. I would recommend knowing people who are comfortable with
      LaTeX in the event of a major problem (thanks Hadrien).

[^1]:
    Mostly from the teacher in charge and the expert but also from the
    BeeScreens team and/or the student
