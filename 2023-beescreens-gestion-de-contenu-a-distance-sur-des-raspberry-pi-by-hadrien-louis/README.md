# BeeScreens - Gestion de contenu à distance sur des Raspberry Pi

Hadrien Louis, 2023

Depuis 2018, le Baleinev Festival propose à ses festivaliers un concept innovant
nommé BeeScreens qui consiste à utiliser certaines fenêtres du bâtiment de la
HEIG-VD comme écrans géants affichant du contenu interactif.

Lors de cet événement, des Raspberry Pi sont utilisés pour afficher sur les
écrans diverses applications. Lors de la dernière édition du festival Baleinev,
plus de 40 Raspberry Pi ont été utilisés en simultané et disposés dans
différents lieux de l'école.

Dans l'état actuel, les Raspberry Pi doivent être installés et configurés
manuellement. Une fois mis en place dans les salles de cours, leur gestion
demande une intervention physique pour vérifier leur bon fonctionnement, le
changement de configuration ou le changement de contenu à afficher ce qui rend
le processus actuel d'administration très contraignant.

L'objectif de ce travail est de créer un système permettant de gérer à distance
l'installation et la configuration des Raspberry Pi afin de simplifier le
processus d'administration autant avant que pendant le festival.

L'application réalisée est composée de trois parties : une API conçue avec
NestJS, un panneau de contrôle créé avec Next.js et Tailwind CSS ainsi qu'un
agent écrit en Go qui est installé sur chaque Raspberry Pi.

L'interface du panneau de contrôle se veut simple et intuitive pour permettre
aux administrateurs de gérer tous les Raspberry Pi et leurs configurations à
distance durant le festival.

La tâche de l'agent est d'interroger régulièrement l'API afin de savoir si de
nouvelles configurations sont à appliquer à l'aide de Ansible.

Pour finir, cette application sera améliorée et utilisée lors de la prochaine
édition du Baleinev Festival. Cet outil va grandement améliorer la gestion de
l'infrastructure et ainsi rendre le festival encore plus interactif grâce à une
gestion en temps réel de tous les écrans.

---

- Teacher in charge: Juergen Ehrensberger
- Expert: Olivier Liechti
- Topic proposed by: Association Baleinev (Ludovic Delafontaine) and Hadrien
  Louis
- Official publication of the thesis: N/A
- BeeScreens documentation: N/A - Work in progress
- Documents
  - First formulation of specifications:
    [`first-formulation-of-specifications.pdf`](./first-formulation-of-specifications.pdf)
  - Intermediary report: [`intermediary-report.pdf`](./intermediary-report.pdf)
  - Final report: [`final-report.pdf`](./final-report.pdf)
  - Poster: [`poster.pdf`](./poster.pdf)
  - Presentation: [`presentation.pdf`](./presentation.pdf)
- Source code:
  - Thesis work:
    <https://gitlab.com/beescreens/beescreens/-/tree/main/apps/dashboard>
  - Thesis report: <https://gitlab.com/hadrylouis/tb-thesis>
- Feedback[^1]:
  - What was good:
    - Very well written and structured
    - Good amount of details without doing too much
    - Covers the entire aspects of the application
    - The presentation was excellent
  - What could have been improved:
    - No need to argument too much about the technologies when joining an
      existing stack and team
    - A summary of how should we set up the Raspberry Pi at the end of the work
      would have been nice to better understand the whole process:
      - Install and configure the base system of the Raspberry Pi
      - Download and configure beegent with the systemd service file
      - Create the device in the Dashboard with its API key from the CPU serial
        number
      - Add a new Playbook on the Git registry
      - Create the configuration in the Dashboard using the previous Playbook
      - Associate the new configuration to the device
      - Wait for the device to synchronize with the Dashboard
      - The device should apply the configuration using Ansible Pull
      - Done
  - Personal feedback and/or tips for future students:
    - Create a project planing (Gantt) to have a better overview of the work that needs to be done and to better manage the time. Especially for the part-time and use a more agile approach for the full-time part.
    - I personally find the following milestones useful for the Gantt
      - Base of the project
      - Analysis
      - Proof of concept
      - Implementation
      - Spare time for the report and the unexpected
    - Use Latex for the report even if you're not familiar with it. It looks more professionnal and it's easier to write than with Word. It is also git friendly.
    - Write the report step by step during the work and not at the end. It's easier to remember what you did and it's less work at the end.
    - If possible plan a weekly meeting with the teacher to discuss about the progress of the work and to get some feedback.

[^1]:
    Mostly from the teacher in charge and the expert but also from the
    BeeScreens team and/or the student
