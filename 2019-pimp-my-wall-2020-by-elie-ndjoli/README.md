# Pimp My Wall 2020

Elie Jacob N'Djoli Bohulu, 2019

Pimp My Wall est un système interactif permettant de dessiner sur des écrans
géants disposés sur la façade de la Haute Ecole d'Ingénierie et de Gestion du
Canton de Vaud (HEIG-VD) depuis un smartphone ou depuis n'importe quel
ordinateur.

L'objectif de ce travail de Bachelor a été de développer une solution pour
l'édition Baleinev de 2019. Il a été nécessaire d'évaluer les besoins en
comparaison avec les éditions précédentes afin de rendre son utilisation plus
conviviale et flexible. Une fois définis, le matériel et le logiciel ont été
implémentés et des tests validés.

La nouvelle version de Pimp my Wall a été un véritable succès surtout du point
de vue des organisateurs et des administrateurs du festival et a grandement
amélioré l'expérience utilisateur.

Les étapes qui ont été réalisées dans ce travail sont les suivantes :

- Définition d'un modèle d'infrastructure permettant de transformer le bâtiment
  de la HEIG-VD en écrans géants ;
- Définition des besoins matériels et logiciels de la mise en place de Pimp My
  Wall ;
- Configuration et administration d'une vingtaine de Raspberry Pi 4 ;
- Création et déploiement de conteneurs Docker.

---

- Teacher in charge: Romuald Mosqueron
- Expert: Unknown
- Topic proposed by: Association Baleinev (Ludovic Delafontaine)
- Official publication of the thesis: <https://tb.heig-vd.ch/7846>
- BeeScreens documentation: N/A - The work was not integrated into BeeScreens
- Documents
  - First formulation of specifications:
    [`first-formulation-of-specifications.pdf`](./first-formulation-of-specifications.pdf)
  - Intermediary report: Unavailable
  - Final report: [`final-report.pdf`](./final-report.pdf)
  - Poster: [`poster.pdf`](./poster.pdf)
  - Presentation: Unavailable
- Source code:
  - Thesis work: <https://gitlab.com/Ndjoli/pimpmywall>
  - Thesis report: Unavailable
- Feedback[^1]:
  - What was good:
    - Very autonomous
    - Good communication
    - Good work
    - Allowed Baleinev to ensure Raspberry Pi 4 compatibility
  - What could have been improved:
    - The work was not integrated into BeeScreens (the project was not mature
      enough to allow to follow Elie's work and integrate it into BeeScreens)
  - Personal feedback and/or tips for future students:
    - Unknown

[^1]:
    Mostly from the teacher in charge and the expert but also from the
    BeeScreens team and/or the student
