# Title of the thesis

Author of the thesis, Year of completion of the thesis

Summary of the thesis

---

- Teacher in charge: Name of the teacher in charge
- Expert: Name of the expert
- Topic proposed by: Name of the person or entity who proposed the topic
- Official publication of the thesis (if applicable): Link to the thesis
  published on <https://tb.heig-vd.ch>
- BeeScreens documentation: Link to the documentation on
  <https://docs.beescreens.ch>
- Documents
  - First formulation of specifications:
    [`first-formulation-of-specifications.pdf`](./first-formulation-of-specifications.pdf)
  - Intermediary report: [`intermediary-report.pdf`](./intermediary-report.pdf)
  - Final report: [`final-report.pdf`](./final-report.pdf)
  - Poster: [`poster.pdf`](./poster.pdf)
  - Presentation: [`presentation.pdf`](./presentation.pdf)
- Source code:
  - Thesis work (if applicable): Link to the source code of the thesis work
  - Thesis report (if applicable): Link to the source code of the thesis report
  - Thesis presentation (if applicable): Link to the source code of the thesis presentation
- Feedback[^1]:
  - What was good:
    - ...
  - What could have been improved:
    - ...
  - Personal feedback and/or tips for future students:
    - ...

[^1]:
    Mostly from the teacher in charge and the expert but also from the
    BeeScreens team and/or the student
