# BeeScreens - Analyse et implémentation de méthodes d'authentification

Mélissa Gehring, 2024

Depuis plus de 30 ans, le campus de la HEIG-VD est animé chaque année par le festival de musique Baleinev. En 2014 naît Pimp My Wall, une application interactive permettant au public de dessiner en temps réel sur les fenêtres des tours de l'école, depuis leurs téléphones. Au fil des ans, Pimp My Wall a été beaucoup retravaillé et d'autres applications sont venues s'y greffer, formant ainsi l'écosystème de BeeScreens.

Actuellement, BeeScreens est composé de quatre applications web partageant de nombreuses caractéristiques communes. Chaque application requiert diverses opérations de modérations, et l'accès à certaines ressources nécessite d'être sécurisé par le biais d'authentification.

L'objectif de ce travail est d'étudier les méthodes d'authentification actuellement mises en place dans BeeScreens, et de proposer une solution faisant usage d'un logiciel dit de gestion d'identité et d'accès, afin de sécuriser l'infrastructure déjà présente et de faciliter le développement de futures applications.

Dans un premier temps, il a fallu analyser les différentes solutions d'authentification actuellement proposées sur le marché, et les évaluer selon plusieurs critères techniques globaux afin d'en sélectionner trois à étudier et tester plus en profondeur.

Dans un deuxième temps, l'étude approfondie et la réalisation de tests sur les trois solutions sélectionnées a permis de déterminer quelle solution était la plus adaptée aux besoins de BeeScreens, et le choix s'est porté vers Authentik.

Finalement, la dernière étape a consisté en le développement d'une application web utilisant la solution Authentik, fournissant aux collaborateurs BeeScreens un exemple d'implémentation d'authentification sécurisée. L'application Beethentik vise donc à servir de modèle pour de futurs projets, sensibilisant à l'importance des bonnes pratiques de l'authentification dans des infrastructures utilisées par un grand public.

---

- Teacher in charge: Alexandre Duc
- Expert: Filipe Fortunato
- Topic proposed by: Association Baleinev and Mélissa
- Official publication of the thesis (if applicable): N/A
- BeeScreens documentation: N/A
- Documents
  - First formulation of specifications:
    [`first-formulation-of-specifications.pdf`](./first-formulation-of-specifications.pdf)
  - Intermediary report: [`intermediary-report.pdf`](./intermediary-report.pdf)
  - Final report: [`final-report.pdf`](./final-report.pdf)
  - Poster: [`poster.pdf`](./poster.pdf)
  - Presentation: [`presentation.pdf`](./presentation.pdf)
- Source code:
  - Thesis work (if applicable): https://gitlab.com/beescreens/beescreens/-/tree/main/apps/beethentik
  - Thesis report (if applicable): _Template made by Sylvain Pasini - Not authorized to share_
  - Thesis presentation (if applicable): [`presentation.pdf`](./presentation.pdf)
- Feedback[^1]:
  - What was good:
    - Well written
    - Very accessible for people not in the security field
  - What could have been improved:
    - Not enough analysis on security aspects
    - More rigorous methodology
    - More regular workloads
  - Personal feedback and/or tips for future students:
    - Using LaTeX was a good idea but hard to master

[^1]:
    Mostly from the teacher in charge and the expert but also from the
    BeeScreens team and/or the student

