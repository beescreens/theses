# Theses

This repository contains the archive of all Bachelor and Master theses related
to BeeScreens.

A template is available in the [`template`](./template) directory.

You can copy the template to create a new thesis archive with the following
convention: `year-of-completion-of-the-thesis-title-of-the-thesis-by-author`.
Replace all spaces with dashes and remove all special characters.

For example, the archive of the thesis titled "BeeScreens - Collection of
interactive applications for the Web" completed in 2018 by John Doe would be
named
`2018-beescreens-collection-of-interactive-applications-for-the-web-by-john-doe`.

The title and summary of the thesis is written in the language of the thesis.
The rest of the archive is written in English.

Please optimize all PDF files with `ps2pdf` (from
[Ghostscript](https://www.ghostscript.com/) package) with
`ps2pdf input.pdf optimized-output.pdf`.

Please format all Markdown files with [Prettier](https://prettier.io/) with
`npx prettier **/*.md --write --print-width 80 --prose-wrap always`
